import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public vehicles: User[];
  public vehicle: User;
  public email = "";
  public password = "";

  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {

  }

  //https://localhost:5001/api/auth/login

  verify() {
    this.http.post(this.baseUrl + 'api/auth/login',
      {
        "email": this.email,
        "password": this.password
      }
    ).subscribe((response) => {
      sessionStorage.setItem('token', response["token"])
      window.location.href = 'https://localhost:5001/vehicles';
    }, error => console.error(alert("error")));
  }
}

interface User {
  email: string;
  password: number;
}
