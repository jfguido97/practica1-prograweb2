import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent  {

  public users: User[];
  public user: User;
  public idToSave = null;


  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
   this.refresh();
  }


  save() {
    this.http.post(this.baseUrl + 'api/auth/register', this.user).subscribe(() => {
      this.refresh();
    }, error => console.error(error));
  }


  refresh(){
    this.user = {
      email: "jhon@doe.com",
      password: "",
    };
  }  

}


interface User {
  email: string;
  password: string;
 
}